# PRE File Importer

Database Setup:

    > docker volume create pre_pg_data
    > docker run --rm --name=postgis -d -e POSTGRES_USER=dbuser -e POSTGRES_PASS=dbpwd -e POSTGRES_DBNAME=climate -e ALLOW_IP_RANGE=0.0.0.0/0 -p 5432:5432 -v pre_pg_data:/var/lib/postgresql kartoza/postgis:12.1

Environment, in project directory:

    > conda env create
    > conda activate jbasoft-conda-env

Run Tests:

    > pytest -v tests

Usage info:

    > python src/pre.py --help

## Typical usage

To view headers:

    > python src/pre.py data/cru-ts-2-10.1991-2000-cutdown.pre

To create database (add -f to recreate):

    > python src/pre.py data/cru-ts-2-10.1991-2000-cutdown.pre -c climate dbuser dbpwd


To import data (use -ri instead of -i to overwrite an import):

    > python src/pre.py data/cru-ts-2-10.1991-2000-cutdown.pre -l climate dbuser dbpwd -i