# pre.py
import click
try:
    from termcolor import colored
except ImportError:
    colored = None
import re
import psycopg2
from postgis.psycopg import register
from psycopg2.extras import execute_values, execute_batch
import datetime
import collections
import time


def log(message, color=None, new_line=True):
    if color and colored:
        click.echo(colored(message,color),nl=new_line)
    else:
        click.echo(message,nl=new_line)

def convert_grid_to_lonlat(x,y):
    """ Converts Grid Coordinates to Lon Lat Tuple """
    lon = (x*0.5)-180.0
    lat = (y*0.5)-90.0
    return (lon,lat)

def convert_grid_polygon(x,y):
    """ Converts Grid Coordinates to WKT Polygon """
    # "POLYGON((-180 -90,-180 -89.5,-179.5 -89.5,-179.5 -90,-180 -90))"
    lon, lat = convert_grid_to_lonlat(x,y)
    p1 = (lon-0.5, lat-0.5)
    p2 = (lon-0.5, lat)
    p3 = (lon, lat)
    p4 = (lon, lat-0.5)
    p5 = p1
    return f"POLYGON(({p1[0]} {p1[1]}, {p2[0]} {p2[1]}, {p3[0]} {p3[1]}, {p4[0]} {p4[1]}, {p5[0]} {p5[1]}))"

def print_header(pre_file, line_counter):
    year_start, year_end = None, None
    log("HEADER:",'blue')
    for line in pre_file:
        log(line, 'green', False)
        line_counter+=1
        # [Boxes=   67420] [Years=1991-2000] [Multi=    0.1000] [Missing=-999]
        if "Years" in line:
            year_start, year_end = re.findall(r"Years=([0-9]+)-([0-9]+)", line)[0]
        if line_counter > 5: #only print header
            break
    return int(year_start), int(year_end)

def db_login_error():
    log("Use --login, -l to add DB credentials", 'red')
    exit(1)

@click.command()
@click.argument('pre_file', type=click.File('r'))
@click.option('--createdb', '-c', 'createdb', help='Creates the PostGIS database: db_name db_user db_password', flag_value='force', default=False)
@click.option('--force', '-f', 'force', help='Forces DB Creation.', flag_value='force', default=False)
@click.option('--login', '-l', 'login', help='Logs in to the database: db_name db_user db_password', type=(str, str, str), default=(None, None, None))
@click.option('--insert', '-i', 'insert', help='Inserts file in database.', flag_value='insert', default=False)
@click.option('--reinsert', '-ri', 'reinsert', help='Force overwrite in database.', flag_value='reinsert', default=False)
def main(pre_file, createdb, force, login, insert, reinsert):
    """ Main Parameterised Function """
    # log("{}, {}, {}, {}, {}".format(pre_file_name, createdb, insert, force, years),'green')

    line_counter = 1

    year_start, year_end = print_header(pre_file, line_counter)
    year_range = year_end - year_start

    db = None

    if login != (None, None, None):
        try:
            db = psycopg2.connect("dbname='{}' user='{}' host='localhost' password='{}' port='5432'".format(login[0],login[1],login[2]))
            register(db)  
        except psycopg2.OperationalError as e:
            log(e, 'red')
            exit(1)

    if createdb:
            if db:
                cur = db.cursor()
                if force: 
                    cur.execute('DROP TABLE IF EXISTS pre ;')
                try:
                    cur.execute('CREATE TABLE pre ("Xref" SMALLINT NOT NULL, "Yref" SMALLINT NOT NULL, "Date" DATE NOT NULL, "Value" SMALLINT, "grid" geography(POLYGON,4326), PRIMARY KEY ("Xref", "Yref", "Date"));')
                    db.commit()
                    log("Table Created", 'yellow')
                except psycopg2.errors.DuplicateTable:
                    log("Table Already Exists, use -f to force recreate", 'red')
                    exit(1)
            else:
                db_login_error()

    if insert or reinsert:
        if not db:
            db_login_error()
        cur = db.cursor()
        SQL = 'INSERT INTO pre ("Xref", "Yref", "Date", "Value", "grid") VALUES %s;'
        SQL_delete = 'DELETE FROM pre WHERE "Xref" = %s AND "Yref" = %s AND "Date" = %s'
        argslist = []
        for line in pre_file:
            line_counter += 1
            if "Grid-ref" in line:
                #log(line, 'green', False)
                # Grid-ref= 142, 268
                grid = line.split("=")[1].split(",")
                x = int(grid[0].strip())
                y = int(grid[1].strip())
                # log(grid)
                #data = (x, y, date,  )
                #log(convert_grid_polygon(x,y))
                years_counter = 0
                # if line_counter > 10:
                #     break
                while years_counter <= year_range:
                    line2 = pre_file.readline()
                    line_counter += 1
                    # log(line2)
                    values = [ v.strip() for v in line2.split(" ") if v.strip() !='']
                    # log(values)
                    for i, value in enumerate(values):
                        if int(value) > 32767 or int(value) < -32768:
                            log("Possible error found on line: " + str(line_counter) +", value: " + str(value) +" NOT inserted", "red" )
                            continue
                        date = datetime.datetime(year_start+years_counter,i+1,1)
                        data = (x,y, date,int(value), convert_grid_polygon(x,y))
                        argslist.append(data)
                       
                        #log(data)
                        #cur.execute(SQL, data)
                    years_counter += 1
        log(f"Inserting {str(len(argslist))} lines, please wait", "green" )
        tic = time.perf_counter()
        if reinsert:
            deletelist = [(x,y,date) for (x, y, date, _ , _) in argslist]
            execute_batch(cur, SQL_delete, deletelist, page_size=10000)
            db.commit()
            log(f"{str(len(deletelist))} lines deleted in {time.perf_counter() - tic:0.4f} seconds", "green" )
        try:
            execute_values(cur, SQL, argslist, page_size=10000)
            db.commit()
        except psycopg2.errors.UniqueViolation:
            log(f"Error: some values already exist in the database, use option -ri instead of -i", "red")
            exit(1)
        
        
        toc = time.perf_counter()
        log(f"Inserted in {toc - tic:0.4f} seconds", "green")


if __name__ == "__main__":
    main()