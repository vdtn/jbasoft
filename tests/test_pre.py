# test_pre.py
import subprocess
import sys
import os
sys.path.append(os.path.relpath("src"))
import pre
import psycopg2
from postgis.psycopg import register

file_path = "data"
file_name = "cru-ts-2-10.1991-2000-cutdown.pre"
file_full_path = file_path + "/" + file_name

db_name = "climate"
db_user = "dbuser"
db_pwd = "dbpwd"


def capture_process(command):
    """ Runs a process """
    proc = subprocess.Popen(command,
        stdout = subprocess.PIPE,
        stderr = subprocess.PIPE,
    )
    out,err = proc.communicate()
    return out, err, proc.returncode

def test_no_args():
    """ No arguments given """
    command = ["python3", "src/pre.py"]
    out, err, exitcode = capture_process(command)
    assert exitcode == 2
    assert out == b''
    assert b"Missing argument" in err

def test_header():
    """ Tests prints header """
    command = ["python3", "src/pre.py", file_full_path]
    out, err, exitcode = capture_process(command)
    assert exitcode == 0
    assert b'''HEADER:
Tyndall Centre grim file created on 22.01.2004 at 17:57 by Dr. Tim Mitchell
.pre = precipitation (mm)
CRU TS 2.1
[Long=-180.00, 180.00] [Lati= -90.00,  90.00] [Grid X,Y= 720, 360]
[Boxes=   67420] [Years=1991-2000] [Multi=    0.1000] [Missing=-999]\n''' in out
    assert err == b''

def test_createdb_new():
    """ Tests Create New Database """
    command = ["python3", "src/pre.py", file_full_path,"--createdb","--login", db_name, db_user, db_pwd, "--force"]
    out, err, exitcode = capture_process(command)
    assert exitcode == 0
    assert b'''Table Created\n''' in out
    assert err == b''

def test_createdb_exists():
    """ Tests Database Exists """
    command = ["python3", "src/pre.py","--createdb","--login", db_name, db_user, db_pwd, file_full_path]
    out, err, exitcode = capture_process(command)
    assert exitcode == 1
    assert b'''Table Already Exists''' in out
    assert err == b''

def test_createdb_wrong_credentials():
    """ Tests Wrong Credentials for Database """
    command = ["python3", "src/pre.py","--createdb","--login", db_name, "sdf", "sdfsdf", file_full_path]
    out, err, exitcode = capture_process(command)
    assert exitcode == 1
    assert b'authentication failed' in out
    assert err == b''

def test_createdb_database_does_not_exist():
    """ Tests Error in Database Name """
    err_db_name = b'ssdf'
    command = ["python3", "src/pre.py","--createdb","--login", err_db_name, db_user, db_pwd, file_full_path]
    out, err, exitcode = capture_process(command)
    assert exitcode == 1
    assert b'database "'+err_db_name+b'" does not exist' in out
    assert err == b''

def test_convert_grid_to_lonlat():
    """ Tests Converts Grid Coordinates to Lon Lat Tuple """
    assert pre.convert_grid_to_lonlat(1,1) == (-179.5, -89.5)
    assert pre.convert_grid_to_lonlat(720,360) == (180.0,90.0)
    assert pre.convert_grid_to_lonlat(10,10) == ((10*0.5)-180.0,(10*0.5)-90.0)
    assert pre.convert_grid_to_lonlat(360,180) == (0,0)

def test_convert_grid_polygon():
    """ Tests Converts Grid Coordinates to WKT Polygon """
    assert pre.convert_grid_polygon(1,1) == "POLYGON((-180.0 -90.0, -180.0 -89.5, -179.5 -89.5, -179.5 -90.0, -180.0 -90.0))"
    assert pre.convert_grid_polygon(360,180) == "POLYGON((-0.5 -0.5, -0.5 0.0, 0.0 0.0, 0.0 -0.5, -0.5 -0.5))"
    assert pre.convert_grid_polygon(720,360) == "POLYGON((179.5 89.5, 179.5 90.0, 180.0 90.0, 180.0 89.5, 179.5 89.5))"

def test_records_written():
    """ Tests Insert works from fresh database """
    command = ["python3", "src/pre.py", file_full_path,"--login", db_name, db_user, db_pwd, "-i","--createdb","-f"]
    out, err, exitcode = capture_process(command)
    assert exitcode == 0
    db = psycopg2.connect("dbname='{}' user='{}' host='localhost' password='{}' port='5432'".format(db_name,db_user,db_pwd))
    register(db)
    cur = db.cursor()
    cur.execute("SELECT * FROM pre;")
    records = cur.fetchall()
    assert 627110 == len(records)

def test_records_overwritten():
    """ Tests Insert works from previous insert """
    command = ["python3", "src/pre.py", file_full_path,"--login", db_name, db_user, db_pwd, "-ri"]
    out, err, exitcode = capture_process(command)
    assert exitcode == 0
    db = psycopg2.connect("dbname='{}' user='{}' host='localhost' password='{}' port='5432'".format(db_name,db_user,db_pwd))
    register(db)
    cur = db.cursor()
    cur.execute("SELECT * FROM pre;")
    records = cur.fetchall()
    assert 627110 == len(records)